const notification = document.querySelector("#notification")

window.addEventListener("load", async () => {
    var response  = await fetch("https://api.realm.tk/handler.php").then(response => response.json());

    let allPassed = true;

    for (let server in response) {
        let iconElement = document.querySelector(`#${server}`);
        iconElement.classList.remove("orange");

        if (response[server] === "UP") {
            iconElement.classList.add("green");
        } else {
            iconElement.classList.add("red"); 
            allPassed = false;
        } 
    }

    notification.classList.remove("orange"); 

    if (allPassed) {
        notification.innerHTML = "All Systems are working!";
    } else {
        notification.innerHTML = "Some Systems are NOT working!";
        notification.classList.add("red"); 
    }
});
